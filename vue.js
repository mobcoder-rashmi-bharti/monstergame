function getRandomValue(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}
// currentRound:0,
const app = Vue.createApp({
  data() {
    return {
      playerHealth: 100,
      monsterHealth: 100,
      currentRound: 0,
      winner: null,
      logMessages: [],
    };
  },
  computed: {
    monsterBarStyle() {
      if (this.monsterHealth < 0) {
        return { width: "0%" };
      }
      return { width: this.monsterHealth + "%" };
    },

    playerBarStyle() {
      if (this.playerHealth < 0) {
        return { width: "0%" };
      }

      return { width: this.playerHealth + "%" };
    },
    mayUseSpecialAttack() {
      return this.currentRound % 3 !== 0;
    },
  },
  watch: {
    playerHealth(value) {
      if (value <= 0 && this.monsterHealth <= 0) {
        //A draw
        this.winner = "draw";
      } else if (value <= 0) {
        //player lost
        this.winner = "monster";
      }
    },
    monsterHealth(value) {
      if (value <= 0 && this.playerHealth <= 0) {
        //A draw
        this.winner = "draw";
      } else if (value <= 0) {
        // monster lost]
        this.winner = "player";
      }
    },
  },
  methods: {
    startGame() {
      (this.playerHealth = 100),
        (this.monsterHealth = 100),
        (this.currentRound = 0),
        (this.winner = null);
      this.logMessages = [];
    },
    attckMonster() {
      this.currentRound++;
      const attckValue = getRandomValue(5, 12);
      this.monsterHealth -= attckValue;
      this.addLogMessage("player", "attack", attckValue);
      this.attckPlayer();
    },
    attckPlayer() {
      const attckValue = getRandomValue(8, 15);
      this.playerHealth -= attckValue;
      this.addLogMessage("monster", "attack", attckValue);
    },
    specialAttackMonster() {
      this.currentRound++;
      const attckValue = getRandomValue(10, 25);
      this.monsterHealth -= attckValue;
      this.addLogMessage("player", "attack", attckValue);
      this.attckPlayer();
    },
    healPlayer() {
      this.currentRound++;
      const healValue = getRandomValue(8, 20);
      if (this.playerHealth + healValue > 100) {
        this.playerHealth = 100;
      } else {
        this.playerHealth += healValue;
      }
      this.addLogMessage("player", "heal", healValue);
      this.attckPlayer();
    },
    surrender() {
      this.winner = "monster";
    },
    addLogMessage(who, what, value) {
      this.logMessages.unshift({
        actionBy: who,
        actionType: what,
        actionValue: value,
      });
    },
  },
});
app.mount("#game");
